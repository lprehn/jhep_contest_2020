## General Setup

The Internet consists of many autonomously organized networks that exchange reachability information for certain sets of IP addresses. While IP addresses are usually aggregated into, so-called, prefixes, networks are often referred to as autonomous systems (ASes). An AS **A** can announce to another AS **B** (i.e., one of **A**'s neighbors) that **B** can send data through **A** to a given target prefix; **B** can further distribute this information to its neighbors, e.g., AS **C**. While doing so, **B** prepends itself to the list of ASes that need to be traversed to reach the prefix which generates the AS Path: ***B A***---the AS that initially announced the prefix, also called the "Origin", can always be found in the right-most position of an AS Path.
When **C** receives this path it might be able to choose from a set of alternative paths, e.g., the AS Path ***E D A***. To choose its preferred alternative, **C** goes through a set of pre-defined tie-breaker rules. The process of deciding which route (i.e., which path for a given prefix) an AS preferes is usually referred to as a routing decision. While the first tie-breaker is the local preference (i.e., a value that can be set by **C** internally to favor one neighbor over another), the second highest tie-breaker is the length of the AS Path. While there is a long list of further tie-breaker rules, we, for now, only focus on the length of an AS Path. 

## AS Path Prepending

Since all tie-breaker rules are well-known, AS **A** has the potential to affect the routing decisions made by other ASes. While **A** can not influence the local preference of remote ASes, it can artificially increase the length of an AS Path by prepending itself multiple times in order to form, e.g., ***B A A A A***---an AS Path with length five. Following the same example from before, AS **C** can now choose between the AS Paths ***B A A A A*** and ***E D A***. When **C** has no local preferency, it will now favor to send data to **A** via **E** and **D**. The technique that **A** used to affect **C**'s routing decision is called AS Path Prepending (ASPP) and the number of times **A** put itself artificially into an AS Path is called the prepend size, e.g., the prepend size of the AS path ***B A A A A*** is three. ASes use ASPP, e.g., for economical reasons; consider a scenario where an AS has one cheap and one expensive neighbor and it wants to minimize the amount of data that comes through the expensive neighbor to reduce the total cost. 


## Data 

Luckily for the research community, some ~700 ASes provide dumps of all the announcements they observe as well as snapshots of how their current routing table (i.e., the table that tracks all currently important routing decisions) looks like. 

## Policy types per prefix for ASes using mixed policies.

As part of our work, we characterize how ASes utilize prepending. First, we define the set of per-prefix policies.

* no-prepend: no visible prepended route
* uniform: the only visible prepend size is N, where N > 0 
* binary: visible routes either have prepend size M or N, where M, N ≥ 0 and M != N􏰀
* diverse: the number of different prepend sizes in the visible routes exceeds two.

When aggregating prefixes to those ASes that announce them, we observe that half of all ASes use a consistent prepending policy while the other half does use mixed policies among their prefixes (not shown by this plot). Based on this result, we were further interested in the mix of policies for the ASes that use mixed policies. 

![Policy types per prefix for ASes using mixed policies.](../plot/prepending_mixed_policy_types_20200504.pdf)	
For our plot, we first group ASes in four groups by the number of prefixes they originate: 1-10, 11-100, 101-1000, more than 1000. Each group (shown at the top) has its own vertical slice in the plot. At the bottom, we show the histogram for the fraction of prepended prefixes for each AS in the respective group. Finally, we calculate for each AS in a group the fraction of prefixes that are prepended uniform, binary, and diverse; in the middle of the plot, we show a boxplot over those fractions where the median is indicated in black. 

## Takeaways 

We first observe that the more prefixes a prepending-using AS announces the more it tends to prepend many of them (see histograms across groups). Further, we see that mixed policies, regardless of the group, are mostly dominated by binary-prepended prefixes. Interestingly, the fewer prefixes a mixed policy AS announces, the more it tends to prepend many prefixes uniformly---a policy that does not affect the path length differences but still increases the overall path length, i.e., a policy that serves no apparent reason. We also observe that ASes that prepend announce many prefixes tend to use more fine-grained prepending policies, which supports the presumption that "large" ASes carefully engineer how traffic enters their network. 