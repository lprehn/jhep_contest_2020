import sys, datetime,gzip, os
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import matplotlib
import numpy as np 
import pickle
from collections import defaultdict
from matplotlib import gridspec
from matplotlib import cm
from pprint import pprint
from scipy.stats.kde import gaussian_kde
from numpy import linspace
from matplotlib.patches import Rectangle



FILE="../data/prepend_policies_20200504.gz"


def get_3_column_figure():
	'''
	Springer template:
	linewidth: 4.8 inches
	textheight: 7.6 inches

	'''
	font = {'family' : 'normal',
        'size'   : 9}

	matplotlib.rc('font', **font)

	width = 9.6
	figure = plt.figure(figsize = (width/3.0, (width/3.0)))
	return figure 

def addlists(*lists):
	data = []
	for i in range(len(lists[0])):
		thesum = 0
		for j in range(len(lists)):
			thesum += lists[j][i]
		data.append(thesum)
	return data

def get_size_class(N):
	if N < 10:
		return 0 
	elif N < 100:
		return 1 
	elif N < 1000:
		return 2 
	else:
		return 3

def transform_data(policies):
	# <100, >100, >1k, >10k
	frac_prepended = [[],[],[],[]]
	prepend_mixes = [[[],[],[]],[[],[],[]],[[],[],[]],[[],[],[]]]
	for asn in policies:
		pols = defaultdict(int)
		N = len(policies[asn])
		for prefpol in policies[asn]:
			pols[prefpol] += 1
		# multiple policies
		P = len(pols.keys())
		if P > 1:
			pfrac = float(pols[0])/float(N)
			N2 = N-pols[0]		# number of actually prepended prefixes
			f_uni = float(pols[1])/float(N2)
			f_bin = float(pols[2])/float(N2)
			f_div = float(pols[3])/float(N2)

			c = get_size_class(N)
			frac_prepended[c].append(pfrac)
			prepend_mixes[c][0].append(f_uni)
			prepend_mixes[c][1].append(f_bin)
			prepend_mixes[c][2].append(f_div)

	return frac_prepended, prepend_mixes


def state_exists():
	return os.path.isfile('../data/mixed_pols_fracs.pkl') and  os.path.isfile('../data/mixed_pols_mix.pkl')

def save_state(frac_prepended, prepend_mixes):
	with open('../data/mixed_pols_fracs.pkl', 'wb') as f:
		pickle.dump(frac_prepended, f, pickle.HIGHEST_PROTOCOL)
	with open('../data/mixed_pols_mix.pkl', 'wb') as f:
		pickle.dump(prepend_mixes, f, pickle.HIGHEST_PROTOCOL)

def load_state():
	with open('../data/mixed_pols_fracs.pkl', 'rb') as f:
		frac_prepended = pickle.load(f)
	with open('../data/mixed_pols_mix.pkl', 'rb') as f:
		prepend_mixes = pickle.load(f)

	return frac_prepended, prepend_mixes

def get_data(file):

	if state_exists():
		frac_prepended, prepend_mixes = load_state()

	else:
		policies = defaultdict(list)

		with gzip.open(file, 'rt') as INPUT:
			for line in INPUT:
				if line.startswith('#'):
					continue
				pref, orig, nummon, ppsizes, pol = line.split('|')
				if int(nummon) < 200:
					continue
				policies[int(orig)].append(int(pol))

		frac_prepended, prepend_mixes = transform_data(policies)
		save_state(frac_prepended, prepend_mixes)

	return frac_prepended, prepend_mixes

def hide_axis(axis, full=False):
	axis.spines['right'].set_visible(False)
	axis.spines['left'].set_visible(False)
	axis.spines['top'].set_visible(False)
	axis.spines['bottom'].set_visible(False)
	if full:
		axis.xaxis.set_tick_params(which='both', bottom = 'False', top = 'False')
		axis.yaxis.set_tick_params(which='both', left = 'False', right = 'False')
		axis.set_xticks([])
		axis.set_yticks([])

def norm_func(array):
	N = sum(array)
	return [x/float(N) for x in array]

def display(frac_prepended, prepend_mixes):
	fig = get_3_column_figure()
	spec = gridspec.GridSpec(ncols=4, nrows=3, figure=fig, height_ratios=[2, 4,1], hspace = 0.01)


	viridis = cm.get_cmap('viridis')
	colors = [viridis(x) for x in [0.33, 1.0, .66, .0]]
	# fixing all the top level plot things 

	labels = fig.add_subplot(spec[0,:])
	hide_axis(labels, full = True)
	custom_markers = [Rectangle([0,0], 2, 1 , fill = True, color=colors[1]),
					Rectangle([0,0], 2, 1 , fill = True, color=colors[2]),
					Rectangle([0,0], 2, 1 , fill = True, color=colors[3])]
	labels.legend(custom_markers, ['uniform', 'binary', 'diverse'], loc = 'center', bbox_to_anchor = [.36, .73], ncol = 3, fancybox = False, frameon=False)


	spineoffset = -0.03

	top = fig.add_subplot(spec[1,:]) 
	hide_axis(top)
	top.xaxis.set_tick_params(which='both', bottom = 'False', top = 'False')

	top.spines['left'].set_visible(True)
	top.spines['left'].set_smart_bounds(True)
	top.spines['left'].set_bounds(0,1)
	top.spines['left'].set_position(('axes', spineoffset))
	top.spines['bottom'].set_visible(True)
	top.spines['bottom'].set_position(('axes', -0.012))
	top.yaxis.grid(which = 'major')
	top.set_ylabel('Fraction of prefixes\noriginated per AS')
	top.set_xlabel(r'Groups (ASes with $X^{max}_{min}$ prefixes)')
	top.xaxis.set_label_position('top')
	ymin = -0.01
	ymax = 1.05
	top.set_ylim(ymin, ymax)
	top.set_xticks([x for x in [.12, .375, .625, .88]])
	top.set_xticklabels([r'$X^{10}_{1}$', r'$X^{100}_{10}$', r'$X^{1K}_{100}$', r'$X^{\infty}_{1K}$'])
	top.xaxis.tick_top()
	top.set_yticks([0, .2, .4, .6, .8, 1])
	top.xaxis.set_tick_params(which='both', bottom = 'False', top = 'False', pad = 0)
	bot = fig.add_subplot(spec[2,:]) 

	hide_axis(bot)
	bot.xaxis.set_tick_params(which='both', bottom = 'False', top = 'False')
	bot.set_xticks([])
	bot.spines['left'].set_visible(True)
	bot.spines['left'].set_smart_bounds(True)
	bot.spines['left'].set_bounds(0,.5)
	bot.spines['left'].set_position(('axes', spineoffset))
	bot.set_ylabel('EPDF(X)')
	bot.set_xlabel('Fraction of prefixes prepended', labelpad = 22)

	bot.set_yticks([0,0.25,0.5])
	offset = .008
	bot.set_ylim(-offset,.5 + offset)
	bot.invert_yaxis()
	bot.patch.set_alpha(0.0)
	bot.yaxis.grid(which = 'major')
	tops = [fig.add_subplot(spec[1,i]) for i in range(4)]
	bots = [fig.add_subplot(spec[2,i]) for i in range(4)]

	tickobjs = top.yaxis.get_major_ticks()
	tickobjs[0].label1.set_verticalalignment('bottom')
	tickobjs = bot.yaxis.get_major_ticks()
	tickobjs[0].label1.set_verticalalignment('top')

	for i in [0,1,2,3]:
		ax = tops[i]
		#print(i)
		ax.set_ylim(ymin, ymax)
		for j in range(3):
			boxes = ax.boxplot(prepend_mixes[i][j], sym = '', positions = [j], widths = [0.6], patch_artist=True)
			for item in ['boxes', 'whiskers', 'medians', 'fliers', 'caps']:
				if item == 'medians':
					plt.setp(boxes[item], color='black')
				else:
					plt.setp(boxes[item], color=colors[j+1])
		ax.patch.set_alpha(0.0)
		hide_axis(ax, full = True)
		ax.spines['top'].set_visible(True)
		ax.spines['top'].set_bounds(-0.3, 2.3)
		ax.set_xlim(-0.5, 2.5)

		dist_space = linspace(0, 1, 100 )

		ax = bots[i]
		hide_axis(ax, full = True)
		ax.xaxis.set_tick_params(which='both', bottom = 'True', top = 'False')
		ax.spines['bottom'].set_visible(True)
		ax.spines['bottom'].set_smart_bounds(True)
		ax.spines['bottom'].set_bounds(0,1)
		ax.set_xticks([0,0.5,1])
		ax.set_xlim(0,1.05)
		ax.patch.set_alpha(0.0)
		ax.set_xticklabels(['0', '0.5', '1'])
		#ax.set_ylim(0.0,1.0)
		ax.invert_yaxis()

		density, bins = np.histogram(frac_prepended[i], normed=True, density=True)
		unity_density = density / density.sum()
		widths = bins[:-1] - bins[1:]

		bots[i].bar(bins[1:], unity_density, width=widths, color = viridis(0.33))

		'''
		N = sum(frac_prepended[i])
		kde = gaussian_kde([x/N for x in frac_prepended[i]])
		kde_vals= kde(dist_space)
		ax.plot(dist_space, kde(dist_space))
		ax.fill_between(dist_space, kde_vals)
		'''
		bots[i].set_ylim(.5,0)	
		bots[i].spines['bottom'].set_position(('data', 0.6))
		tickobjs = ax.xaxis.get_major_ticks()
		tickobjs[0].label1.set_horizontalalignment('left')
		tickobjs[2].label1.set_horizontalalignment('right')
		#ax.scatter([j]*len(prepend_mixes[i][j]), prepend_mixes[i][j], s = 1, alpha = .1)
		

	spec.tight_layout(fig, h_pad=-3, pad=0, rect=(0, -0.07, 1, 1)) #w_pad=-0-5, h_pad=-0.50)
	plt.autoscale()
	plt.savefig("../plot/prepending_mixed_policy_types_20200504.pdf", bbox_to_inches='tight')

frac_prepended, prepend_mixes = get_data(FILE)
for i in [0,1,2,3]:
	print(len(frac_prepended[i]))
#pprint(prepend_mixes)
display(frac_prepended, prepend_mixes)


