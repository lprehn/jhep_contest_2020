# JohnHunter Excellence in Plotting Contest 2020

This Repository contains multiple submissions to the 2020 edition of the JohnHunter Excellence in Plotting Contest. 
The following directories contain the following submissions:

* ./submission_01/ --- The Evolution of prepend sizes in different regions.
* ./submission_02/ --- The Effect of prepend sizes on Prefix Hijacks.
* ./submission_03/ --- The Effect of prepend sizes on incomming traffic.
* ./submission_04/ --- Policy types per prefix per for ASes using mixed policies.  

All directories have the same structure, they all contain the following sub-directories:

* ./data/ --- contains a file with all the data.
* ./explanation/ --- contains a file explaining the background as well as the plot itself.
* ./plot/ --- contains only the finished plot.
* ./script/ ---  contains the script that produces the plot. 

To invoke the script simply go to the ./script/ sub-directory and run:

~~~
python3 <scriptname>.py
~~~

this will automatically read the data from the respective data sub-directory and produce a plot in the plot sub-directory.<br>
Unfortunately, I hadn't found the time yet to properly document the code due to the deadline beeing to close to ACM IMC's deadline. 

In case you need further information, please reach out: 

website: http://www.whoislarsprehn.net <br>
e-mail: lprehn@mpi-inf.mpg.de
