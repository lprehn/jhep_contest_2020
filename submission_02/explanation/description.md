## General Setup

The Internet consists of many autonomously organized networks that exchange reachability information for certain sets of IP addresses. While IP addresses are usually aggregated into, so-called, prefixes, networks are often referred to as autonomous systems (ASes). An AS **A** can announce to another AS **B** (i.e., one of **A**'s neighbors) that **B** can send data through **A** to a given target prefix; **B** can further distribute this information to its neighbors, e.g., AS **C**. While doing so, **B** prepends itself to the list of ASes that need to be traversed to reach the prefix which generates the AS Path: ***B A***---the AS that initially announced the prefix, also called the "Origin", can always be found in the right-most position of an AS Path.
When **C** receives this path it might be able to choose from a set of alternative paths, e.g., the AS Path ***E D A***. To choose its preferred alternative, **C** goes through a set of pre-defined tie-breaker rules. The process of deciding which route (i.e., which path for a given prefix) an AS prefers is usually referred to as a routing decision. While the first tie-breaker is the local preference (i.e., a value that can be set by **C** internally to favor one neighbor over another), the second-highest tie-breaker is the length of the AS Path. While there is a long list of further tie-breaker rules, we, for now, only focus on the length of an AS Path. 

## AS Path Prepending

Since all tie-breaker rules are well-known, AS **A** has the potential to affect the routing decisions made by other ASes. While **A** can not influence the local preference of remote ASes, it can artificially increase the length of an AS Path by prepending itself multiple times in order to form, e.g., ***B A A A A***---an AS Path with length five. Following the same example from before, AS **C** can now choose between the AS Paths ***B A A A A*** and ***E D A***. When **C** has no local preferency, it will now favor to send data to **A** via **E** and **D**. The technique that **A** used to affect **C**'s routing decision is called AS Path Prepending (ASPP) and the number of times **A** put itself artificially into an AS Path is called the prepend size, e.g., the prepend size of the AS path ***B A A A A*** is three. ASes use ASPP, e.g., for economical reasons; consider a scenario where an AS has one cheap and one expensive neighbor and it wants to minimize the amount of data that comes through the expensive neighbor to reduce the total cost. 

## BGP Hijacking 

A malicious AS **M** might announce the same prefix as AS **A** to receive some of **A**'s data packets. Such a scenario is usually referred to as a prefix hijack. How far a hijack propagates (i.e., how many ASes pick the hijacked route as their best route) often depends on the respective lengths of **A**'s and **M**'s paths. 

## Data 

Luckily for the research community, some ~700 ASes provide dumps of all the announcements they observe as well as snapshots of how their current routing table (i.e., the table that tracks all currently important routing decisions) looks like---henceforth we refer to those ASes as monitors.

## The Effect of prepending on prefix hijacks

As part of our work, we want to understand how prepending can facilitate the wide-spread adoption of prefix hijacks. We emulate 20 different location scenarios and performed hijacks between them; for each scenario, we announce the prefix legitimately with none, one, two, or three prepends. Afterward, we announce the hijack and track the fraction of monitors that choose the hijacked route as the best route. 

![The Effect of prepending on prefix hijacks](../plot/effect_of_prepending_on_hijacks.pdf)

The figure shows i) the location as well as the prepend size of the legitimately announced route---on the x-axis; ii) the amount of upstreams (i.e., neighbors) that the location has---as bars on the top; iii) the location from which the hijack was started---on the y-axis. In each cell, we denote (and color-code) the percentage of monitors that chose the hijacked route as best-route. 

## Takeaways

In general, we observe that with increasing prepend size more monitors adopt the hijacked route. We see that connectivity (i.e., the number of upstreams) has little effect: While Seattle with 33 upstreams was almost always completely hijacked, Gatech and Grnet---both with 4 upstreams---were less consistently hijacked and Amsterdam with 43 upstreams was only hijacked when it used the maximum prepend size. Finally, we observe that three prepends appears to be a threshold after which a hijackers route takes over all monitors, which is likely related to the fact that the average path length observed from all monitors lies somewhere between 3 and 4. 