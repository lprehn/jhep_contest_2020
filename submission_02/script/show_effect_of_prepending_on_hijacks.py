import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
DATAFILE='../data/hijack_exp_results.csv'

def read_data(file):

	x_mapping = {
		'amsterdam01_P0':0,
		'amsterdam01_P1':1,
		'amsterdam01_P2':2,
		'amsterdam01_P3':3,
		'seattle01_P0':4,
		'seattle01_P1':5,
		'seattle01_P2':6,
		'seattle01_P3':7,
		'gatech01_P0':8,
		'gatech01_P1':9,
		'gatech01_P2':10,
		'gatech01_P3':11,
		'grnet01_P0':12,
		'grnet01_P1':13,
		'grnet01_P2':14,
		'grnet01_P3':15,
		'clemson01_P0':16,
		'clemson01_P1':17,
		'clemson01_P2':18,
		'clemson01_P3':19
	}

	y_mapping = {
		'amsterdam01':0,
		'seattle01':1,
		'gatech01':2,
		'grnet01':3,
		'clemson01':4,
	}

	data = np.empty([5,20])
	data.fill(np.nan)

	with open(file, 'r') as INPUT:
		for line in INPUT:
			if line.startswith('#'):
				continue
			_, _, orig, _, leak, _, maxleak, minorig = line.split('|')
			maxleak = float(maxleak)
			minorig = int(minorig)
			if minorig == 1000000:
				data[y_mapping.get(leak), x_mapping.get(orig)] = 0.0
			else:
				data[y_mapping.get(leak), x_mapping.get(orig)] = maxleak/float(maxleak+minorig)*100
	return data, x_mapping, y_mapping

def get_2x3_column_figure():
	'''
	Springer template:
	linewidth: 4.8 inches
	textheight: 7.6 inches

	'''
	font = {'family' : 'normal',
        'size'   : 9}

	matplotlib.rc('font', **font)

	width = 9.6
	figure = plt.figure(figsize = (width/3.0*2, (width/3.0)))
	return figure 

def stylize(axis, width):
	axis.spines['right'].set_visible(False)
	axis.spines['top'].set_visible(False)
	axis.spines['bottom'].set_smart_bounds(True)
	axis.spines['bottom'].set_position(('outward', width)) 
	axis.spines['left'].set_smart_bounds(True)
	axis.spines['left'].set_position(('outward', width)) 

def show(data, x_mapping, y_mapping):
	fig = get_2x3_column_figure()
	

	spec = gridspec.GridSpec(ncols=1, nrows=3, height_ratios = [1,1, 3], figure=fig, hspace = .1)
	ax = fig.add_subplot(spec[2])
	cax = fig.add_subplot(spec[0])
	cax.spines['top'].set_visible(False)
	cax.spines['bottom'].set_visible(False)
	cax.spines['left'].set_visible(False)
	cax.spines['right'].set_visible(False)
	cax.tick_params(labelcolor='w', top='off', bottom='off', left='off', right='off')


	

	cbar = ax.imshow(data, cmap="viridis")
	cbar_rec = fig.add_axes([0.125,0.8,0.777,0.05])
	cbar_ax = fig.colorbar(cbar, ax = cax, cax = cbar_rec, orientation="horizontal")
	cbar_rec.set_xlabel("% of monitors choosing hijacked path as best-path.")
	cbar_rec.xaxis.tick_top()
	for (i, j), value in np.ndenumerate(data):
		if not np.isnan(value):
			ax.text(j, i, "%.0f"%value, va='center', ha='center')

	xpos = list(range(0, 20))
	xticklabs = []
	for N in ['$A', '$S', '$G_A', "$G_R", '$C']:
		for i in range(4):
			xticklabs.append(N+'^'+str(i)+'$')
	yticklabs = ['Amsterdam (A)', 'Seattle (S)', 'Gatech '+r'($G_A$)', 'Grnet' +r'($G_R$)', 'Clemson (C)']
	ax.set_yticks([0,1,2,3,4])
	ax.set_yticklabels(yticklabs)
	ax.set_xticks(xpos)
	ax.set_xticklabels(xticklabs)
	ax.set_xlabel('Actual announcement from location X with Y many prepends, written as $X^Y$')
	ax.set_ylabel('Hijack location')



	cntax = fig.add_subplot(spec[1], sharex = ax )
	cntax.spines['top'].set_visible(False)
	cntax.spines['bottom'].set_visible(True)
	cntax.spines['left'].set_visible(False)
	cntax.spines['right'].set_visible(False)
	cntax.bar([1.5,5.5,9.5,13.5,17.5], [43, 33, 4, 4, 1], width = 3, color = 'lightgrey')
	xheight = 22
	cntax.text(1.5, xheight, 43, va='center', ha='center')
	cntax.text(5.5, xheight, 33, va='center', ha='center')
	cntax.text(9.5, xheight, 4, va='center', ha='center')
	cntax.text(13.5, xheight, 4, va='center', ha='center')
	cntax.text(17.5, xheight, 1, va='center', ha='center')
	cntax.set_ylabel('# Upstreams   ', rotation = 0, ha='right', va = 'center')
	cntax.tick_params(labelcolor='w', top='off', bottom='off', left='off', right='off', labelbottom='off', labelleft = 'off')
	cntax.set_xlim(-0.5, 19.5)

	stylize(ax, 6)
	ax.spines['left'].set_bounds(0,4)
	ax.spines['bottom'].set_bounds(0,19)
	#cntax.spines['bottom'].set_bounds(0,19)
	[row, col] = np.where(np.isnan(data))
	ax.plot(col, row, 'rx', markersize = 15)
	plt.tight_layout()
	plt.savefig('../plot/effect_of_prepending_on_hijacks.pdf', bbox_inches='tight')


data, x_mapping, y_mapping = read_data(DATAFILE)
show(data, x_mapping, y_mapping)
