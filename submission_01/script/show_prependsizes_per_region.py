from collections import defaultdict
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib import cm
from matplotlib.lines import Line2D

def get_data():
	'''
	reads all Data from the respective file. 
	-------------------- INPUTS -------------------------
	@axis: 		The axis that is supposed to be hidden
	@full:		[Optional] whether everything should be hidden
				or only the spines
	-----------------------------------------------------
	@return:	min_data - a default dict containing a list of all
				minimum prepend sizes of a given date 

				max_data - a default dict containing a list of all
				maximum prepend sizes of a given date 

	@Sideff.: 	---
	'''

	# tracking the min and max prepend size for every prefix
	min_data = defaultdict(dict)
	max_data = defaultdict(dict)

	# File that contains the data
	FILE='../data/prependsizes_per_prefix_per_region_over_10_years.csv'

	# reading file
	with open(FILE, 'r') as INPUT:
		for line in INPUT:

			# file has one line per date
			date, *chunks = line.split('|')
			for chunk in chunks:

				# get the region as well as the asssociated values for each region 
				rir, fract, vals = chunk.split(':')
				mins, maxs = vals.split(';')

				# and save it
				min_data[date][rir] = [float(x) for x in mins.split(',')]
				max_data[date][rir] = [float(x) for x in maxs.split(',')]

	return min_data, max_data
            
def get_2x3_column_figure():
	'''
	this method produces a 2/3 linewidth plot
	@return:	the figure

	@Sideff.: 	---
	'''
	font = {'size'   : 9}

	matplotlib.rc('font', **font)

	width = 9.6
	figure = plt.figure(figsize = (width/3.0*2, (width/3.0)))
	return figure 

def hide_axis(axis, full=False):
	'''
	this method hides most/all parts of an axis
	-------------------- INPUTS -------------------------
	@axis: 		The axis that is supposed to be hidden
	@full:		[Optional] whether everything should be hidden
				or only the spines
	-----------------------------------------------------
	@return:	None

	@Sideff.: 	---

	'''

	# sets all spines invisible
	axis.spines['right'].set_visible(False)
	axis.spines['left'].set_visible(False)
	axis.spines['top'].set_visible(False)
	axis.spines['bottom'].set_visible(False)

	if full:
		# gets rid of all ticks and ticklabels 
		axis.xaxis.set_tick_params(which='both', bottom = 'False', top = 'False')
		axis.yaxis.set_tick_params(which='both', left = 'False', right = 'False')
		axis.set_xticks([])
		axis.set_yticks([])

def add_barh(ax, thecmap, min_data, max_data):
	'''
	this method adds a subplot for a single cell onto the provided axis

	-------------------- INPUTS -------------------------
	@ax: 		The axis to add to
	@thecmap:	The cmap that will be used
	@min_data:	the list of minimum prepend sizes
	@max_data:	the list of maximum prepend sizes
	-----------------------------------------------------
	@return:	None

	@Sideff.: 	---
	'''

	# add horizontal bars for min, max, and overlap 
	ax.barh([1,2,3,4], min_data, color = thecmap(1), zorder = 10)
	ax.barh([1,2,3,4], max_data, color = thecmap(0.7), zorder = 11)
	ax.barh([1,2,3,4], [min(min_data[i],max_data[i]) for i in range(len(min_data))], color = thecmap(0.35), zorder = 12)

	# there is no bar larger than 0.65 so we can maximize visibility by reducing the x axis length
	ax.set_xlim(0,0.65)

	# add the individual prepend sizes onto each bar, 
	# with refers to how far the text will be away from the left
	width = .01
	fd = {'size'   : 5}
	ax.text(width, 1, "1", color = 'w', fontdict = fd, verticalalignment='center', zorder = 100)
	ax.text(width, 2, "2", color = 'w',fontdict = fd, verticalalignment='center',  zorder = 100)
	ax.text(width, 3, "3", color = 'w', fontdict = fd, verticalalignment='center',  zorder = 100)
	ax.text(width, 4, "4+", color = 'w', fontdict = fd, verticalalignment='center',  zorder = 100)

	# hide the entire axis
	hide_axis(ax, True)

def show(min_data, max_data):
	'''
	this method produces and saves the plot

	-------------------- INPUTS -------------------------
	@axis: 		The axis that is supposed to be hidden
	@full:		[Optional] whether everything should be hidden
				or only the spines
	-----------------------------------------------------
	@return:	None

	@Sideff.:	Produces the plot at ../plot/
	'''

	# get a figure that has 2/3 width of the springer template and the color map
	fig = get_2x3_column_figure()
	viridis = cm.get_cmap('viridis')

	# defining the structure for all subplots
	spec = gridspec.GridSpec(ncols=11, nrows=5, figure=fig, hspace = .4, wspace = .2)

	# get all dates in a sorted version
	dates = sorted(min_data.keys())

	# we now itarte thropugh dates and rirs and add their ax and subplot
	for colidx, date in enumerate(dates):
		for rowidx, rir in enumerate(['afrinic', 'apnic', 'arin', 'lacnic', 'ripencc']):
			barax = fig.add_subplot(spec[rowidx, colidx])

			# does most of the work
			add_barh(barax, viridis, max_data[date][rir], min_data[date][rir])

	# using a different axis for the actual spines and ticks  
	framing_axis = fig.add_subplot(111)

	# make it transparant
	framing_axis.patch.set_alpha(0)

	# set the xticks and yticks 
	framing_axis.set_ylim(0,10)
	yticks = [.75,2.875,5.0,7.125,9.25] 
	framing_axis.set_yticks(yticks)
	framing_axis.set_yticklabels(['RIPENCC', 'LACNIC', 'ARIN', 'APNIC', 'AFRINIC'])
	framing_axis.set_xlim(-0.5,10.5)
	framing_axis.set_xticks(list(range(11)))
	framing_axis.set_xticklabels(["20"+str(x) for x in range(10,21)])
	framing_axis.yaxis.set_label_position("right")

	# set custom seperator lines 
	for i in range(len(yticks)-1):
		framing_axis.axhline((yticks[i]+yticks[i+1])/2.0, zorder = 1000, color = 'lightgrey', linewidth = 1	)

	# styling the axis 
	framing_axis.spines['top'].set_visible(False)
	framing_axis.spines['right'].set_visible(False)
	framing_axis.spines['left'].set_visible(False)
	framing_axis.spines['bottom'].set_position(('outward', 6)) 
	framing_axis.spines['left'].set_position(('outward', 6)) 
	framing_axis.yaxis.set_tick_params(which='both', length=0)

	# labels 
	framing_axis.set_ylabel("EPDF(prepend size per prefix)\nper region")
	framing_axis.set_xlabel("time (15th Jan., yearly)")

	# provide a customized legend
	boxes = [plt.Rectangle((0, 0), 4, 3, facecolor=viridis(1)),
		plt.Rectangle((0, 0), 4, 3, facecolor=viridis(.7)),
		plt.Rectangle((0, 0), 4, 3, facecolor=viridis(.35))]
	framing_axis.legend(boxes, ['Max Prepend Size', 'Min Prepend Size', 'Overlap'], loc = 'upper center', ncol = 3, handlelength = 1, bbox_to_anchor=(0.5, 1.15), fancybox = False, markerscale = .5, framealpha=0.0)
              
	# removing all borders & saving
	plt.tight_layout()  
	plt.savefig('../plot/ppsize_per_region_over_time.pdf', bbox_to_inches = 'tight')



min_data, max_data = get_data()
show(min_data, max_data)