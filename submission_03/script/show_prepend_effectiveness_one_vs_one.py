import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import gzip
from collections import defaultdict
from matplotlib import cm
from matplotlib.lines import Line2D
FILE='../data/prepending_effectiveness_one_vs_one.csv.gz'

def get_idx(mux):
	global MUXES
	return MUXES.index(mux)

MUXES=['amsterdam01','clemson01','gatech01','grnet01','neu01','seattle01','ufmg01','utah01','uw01','wisc01']

def read_data(file):

	# in numpy the first value is number of rows while the second is number of columns 
	initial = np.full((10,10),-1,dtype = np.float64)
	max_shift = np.full((10,10),-1,dtype = np.float64)
	fracs_one = np.full((10,10),-1,dtype = np.float64)
	fracs_two = np.full((10,10),-1,dtype = np.float64)

	with gzip.open(file, 'rt') as INPUT:
		for line in INPUT:
			if line.startswith('#') or line.startswith('mux_a'):
				continue
			mux_prep, mux_norm, ppzero, ppone, pptwo, ppthree = line.split('|')
			# data represents: prep/prep+norm
			ppzero = float(ppzero)
			ppone = float(ppone)
			pptwo = float(pptwo)
			ppthree = float(ppthree)

			idx_prep = get_idx(mux_prep) 
			idx_norm = get_idx(mux_norm)

			#print(ppzero)
			if ppzero <0.0000001:
				max_shift[idx_prep, idx_norm] = -2 
				initial[idx_prep, idx_norm] = -2
				fracs_one[idx_prep, idx_norm] = -2
				fracs_two[idx_prep, idx_norm] = -2
			else:
				max_shift[idx_prep, idx_norm] = ppzero - ppthree
				initial[idx_prep, idx_norm] = ppzero
				try:
					fracs_one[idx_prep, idx_norm] = float(ppzero-ppone)/float(ppzero - ppthree)
					fracs_two[idx_prep, idx_norm] = float(ppzero-pptwo)/float(ppzero - ppthree)
				except:
					fracs_one[idx_prep, idx_norm] = -3
					fracs_two[idx_prep, idx_norm] = -3
	print(fracs_one[3,2])
	return initial, max_shift, fracs_one, fracs_two


def get_2x3_column_figure():
	'''
	Springer template:
	linewidth: 4.8 inches
	textheight: 7.6 inches

	'''
	font = {'family' : 'normal',
        'size'   : 9}

	matplotlib.rc('font', **font)

	width = 9.6
	figure = plt.figure(figsize = (width/3.0*2, (width/3.0)))
	return figure 

def stylize(axis, width):
	axis.spines['right'].set_visible(False)
	axis.spines['top'].set_visible(False)
	axis.spines['bottom'].set_smart_bounds(True)
	axis.spines['bottom'].set_position(('outward', width)) 
	axis.spines['left'].set_smart_bounds(True)
	axis.spines['left'].set_position(('outward', width)) 

def hide_axis(axis, full=False):
	axis.spines['right'].set_visible(False)
	axis.spines['left'].set_visible(False)
	axis.spines['top'].set_visible(False)
	axis.spines['bottom'].set_visible(False)
	if full:
		axis.xaxis.set_tick_params(which='both', bottom = 'False', top = 'False')
		axis.yaxis.set_tick_params(which='both', left = 'False', right = 'False')
		axis.set_xticks([])
		axis.set_yticks([])

def add_single_subplot(ax, thecmap, zero, one, two, three, max_shift, legend = False):

	#ax.bar([1,2], [one, two], zorder = 10, color = ['lightgrey', 'grey'], alpha = 0.5)
	handles_red, handles_one, handles_two, handles_zero = (None, None, None, None)
	if zero == -1:
		if legend: 
			print('here')
		ax.patch.set_facecolor('lightgray')
		ax.patch.set_alpha(0.5)
		ax.patch.set_hatch('///')
	elif three<0.00001 and zero<0.000001:
		
		handles_red = ax.plot([1,2], [0.5]*2, color = 'r', linewidth = 1.5, label = 'Not shiftable')
	else:
		handles_one = ax.bar([0.75], [max(0,one)], color = ['lightgrey'], alpha = 0.7,label = "Size 1 shift fraction", zorder = 100)
		handles_two = ax.bar([2.25], [max(0,two)], color = ['grey'], alpha = 0.7, label = "Size 2 shift fraction", zorder = 100)
		ax.plot([1.5, 1.5], [0,1], color = 'white', linewidth = 1.5, zorder = 200)
		rect = matplotlib.patches.Rectangle((0,0),3,1, edgecolor=None, facecolor=thecmap((three/max_shift)*(max_shift/0.8)), zorder = 1)
		ax.add_patch(rect)
		handles_zero = ax.plot([0, zero*3.0], [-0.15]*2, color = 'k', linewidth = 1)
		ax.plot([0, 3.0], [-0.15]*2, color = 'k', linewidth = 1, alpha = 0.25)
		#ax.set_facecolor()
	ax.set_ylim(-0.25, 1.0)
	ax.set_xlim(0.0,3.0)
	#print(one)
	#print(two)
	hide_axis(ax, True)

	if legend:
		print(handles_red, handles_one, handles_two)
		handles = []
		if not handles_red is None:
			handles.extend(handles_red)
		if not handles_zero is None:
			handles.extend(handles_zero)
		if not handles_one is None:
			handles.extend(handles_one)
		if not handles_two is None:
			handles.extend(handles_two)

		return handles
	

def add_x_cell(ax):
	color = 'r'
	lw = 1.5
	ax.plot([0.6,1.4],[0.15,0.85], color = color, linewidth = lw)
	ax.plot([0.6,1.4],[0.85,0.15], color = color, linewidth = lw)
	ax.set_ylim(0.0,1.0)
	ax.set_xlim(0.0,2.0)
	hide_axis(ax, True)

def show(initial, max_shift, fracs_one, fracs_two):
	fig = get_2x3_column_figure()
	spec = gridspec.GridSpec(ncols=12, nrows=10, figure=fig)
	#spec.update(wspace=0.2, hspace=0.2)
	viridis = cm.get_cmap('viridis')

	MAX_SHIFT = max_shift.max()
	print(MAX_SHIFT)
	
	#img = plt.imshow(therange, cmap=viridis)
	#cbar_ax = fig.colorbar(img, ax = cbax, cax = cbar_frame, orientation="vertical")

	handles = []
	for rowidx in range(10):
		for colidx in range(10):
			barax = fig.add_subplot(spec[rowidx, colidx])
			if colidx == rowidx:
				add_x_cell(barax)
			else:
				if (rowidx == 7 or rowidx == 8) and colidx == 0:
					handles.extend(add_single_subplot(barax, viridis,initial[rowidx, colidx], fracs_one[rowidx, colidx], fracs_two[rowidx, colidx], max_shift[rowidx, colidx], MAX_SHIFT, True))

				else:
					add_single_subplot(barax, viridis,initial[rowidx, colidx], fracs_one[rowidx, colidx], fracs_two[rowidx, colidx], max_shift[rowidx, colidx], MAX_SHIFT, False)

	global MUXES
	framing_axis = fig.add_subplot(spec[:, :10])
	framing_axis.patch.set_alpha(0)
	framing_axis.set_ylim(0,10)
	framing_axis.set_xlim(0,10)
	rescale = 0.017
	yticks = [x+0.6+((x-5)*rescale) for x in range(10)]
	xticks = [x+0.5+((x-5)*rescale) for x in range(10)]
	framing_axis.set_yticks(yticks)
	framing_axis.set_xticks(xticks)
	yticklabels = MUXES
	xticklabels = [" "]*10
	for i in range(len(MUXES)):
		if i == 8:
			yticklabels[i] = r"Uw ($U_{W}$)"
			xticklabels[i] = r"$U_{W}$"
			continue
		if i == 7:
			yticklabels[i] = r"Utah ($U_{T}$)"
			xticklabels[i] = r"$U_{T}$"
			continue
		if i == 6:
			yticklabels[i] = r"Ufmg ($U_{F}$)"
			xticklabels[i] = r"$U_{F}$"
			continue
		if i == 3:
			yticklabels[i] = r"Grnet ($G_{R}$)"
			xticklabels[i] = r"$G_{R}$"
			continue
		if i == 2:
			yticklabels[i] = r"Gatech ($G_{A}$)"
			xticklabels[i] = r"$G_{A}$"
			continue
		yticklabels[i] = yticklabels[i][0].upper() + yticklabels[i][1:-2]+" ("+yticklabels[i][0].upper()+")"
		xticklabels[i] = yticklabels[i][0].upper()

	framing_axis.set_yticklabels(yticklabels[::-1])
	framing_axis.set_xticklabels(xticklabels)
	stylize(framing_axis, 6)
	framing_axis.tick_params(axis=u'both', which=u'both',length=0)
	framing_axis.set_xlabel("Not prepended PoP")
	framing_axis.set_ylabel("Prepended PoP", labelpad = -8)


	therange = [[x] for x in np.linspace(0.0, 1.0, 100)]
	cbax = fig.add_subplot(spec[:, 10])
	#cbax.set_facecolor('r')

	hide_axis(cbax, full = True)
	margin = 0.002
	cbax.set_ylim(-margin,1+margin)
	cbax.set_xlim(0,1)
	img = cbax.imshow(therange, cmap=viridis, interpolation='nearest', extent = [0,0.3,0,1], zorder = 10, origin = 'lower')
	for i in range(4):
		if i in [1,2]:
			cbax.plot([0.3, 0.4], [i*0.3333334]*2, color = 'black', zorder = 1, linewidth = 1)
		else:
			cbax.plot([0.0, 0.4], [i*0.3333334]*2, color = 'black', zorder = 100, linewidth = 1)
		cbax.text(0.5, i*0.3333334, "%.1f" % (i*0.3), verticalalignment = 'center', zorder = 10)
	cbax.set_aspect('auto')
	#framing_axis.set_yticklabels(['RIPENCC', 'LACNIC', 'ARIN', 'APNIC', 'AFRINIC'])

	cmap_label_ax = fig.add_subplot(spec[:, 11])
	cmap_label_ax.set_ylim(0,1)
	cmap_label_ax.set_xlim(0,1)
	hide_axis(cmap_label_ax, full = True)
	cmap_label_ax.text(0.5, 0.5, "Fraction of possibly movable sources\n moved with max prepend size", rotation = 270, verticalalignment = 'center', horizontalalignment='center')

	'''
	legend_ax = fig.add_subplot(spec[0, :])
	hide_axis(legend_ax, full = True)
	legend_ax.legend(handles, ["Not shiftable","PSTF","CPS1","CPS2"], loc='center', handlelength=1, ncol = 4, bbox_to_anchor= (0.4, 0.5), frameon = False)
	'''
	plt.tight_layout(pad=0.25, w_pad=0.1, h_pad=0.1)  
	plt.savefig('../plot/prepend_effectiveness_one_vs_one.pdf', bbox_to_inches = 'tight')

initial, max_shift, fracs_one, fracs_two = read_data(FILE)
show(initial, max_shift, fracs_one, fracs_two)
